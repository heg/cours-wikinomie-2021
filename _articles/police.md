---
layout: theme
title: "Police"
key: police
is_main: true
toc:
- "Contexte"
---

### Contexte
La recherche de notre groupe concernait deux services de l'État de Genève. Nous avons alors reçu deux activités différentes. La première concernait l’organe de médiation de la police et la seconde portait sur la cybercriminalité. 
 
L'organe de médiation de la police (OMP) est un organisme qui se trouve entre les citoyens et la police, il est la balance entre les conflits qu’il peut potentiellement y avoir entre eux. En effet, il est très difficile de rester neutre et de ne pas faire pencher la balance d’un côté ou d’un autre, de toujours garder le cap sur la résolution du conflit afin que les deux parties en ressortent satisfaites de l’analyse finale.
 
Le service de cybercriminalité de la police à Genève collabore avec les citoyens pour la prévention d’éventuelle cyber-attaque récurrente dans les foyers de Genève, à l’aide de leur cyberkiosque qui est mis à disposition des citoyens. Dû à la grande numérisation qui est arrivée récemment dans les différentes entreprises, le service de police se doit de collaborer avec différentes PME afin de lutter contre ce fléau.
