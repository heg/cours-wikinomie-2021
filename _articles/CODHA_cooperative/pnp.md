---
layout: activity
key: CODHA_cooperative
title: Les PNP
tags: [collaboration, logement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle

Dans cette activité, nous nous intéressons à la gestion et au fonctionnement d'une seule coopérative. Toutes les coopératives de la CODHA fonctionnent sur le même modèle, cette activité s'adapte donc à n'importe quelle coopérative.

Chaque coopérative est auto-gérée par ses habitants, comme nous l'a expliqué Mr. Käser :

> Chaque coopérative se partage les tâches de régie entre les différents habitants. Le budget aloué est plus ou moins égal à celui d'une régie classique. Les habitants s'organisent en groupe de travail afin de se pencher sur les différents points nécessaires au bon fonctionnement de la coopérative.

Pendant longtemps, il n'existait pas de support informatique afin d'aider à la gestion des coopératives. Les habitants se réunissaient, prenaient des décisions et consignaient les décisions prises dans un PV qui terminait dans un classeur.

Afin de simplifier et d'accélérer la gestion des coopératives, un outil nommé PNP (Plateforme Numérique Participative) a été créé. Sur cet outil, auqel chaque habitant de la coopérative a accès, il est possible d'effectuer les actions suivantes :

- Déposer ou consulter des plans d'architecte afin d'obtenir des retours
- Déposer ou consulter des PVs qui consignent le contenu des réunions
- Déposer ou consulter des documents en tout genre 
- Utiliser des carnets d'adresses afin de faire parvenir un message à une certaine catégorie de personnes
- Réserver une salle dans la coopérative
- Signaler des problèmes techniques

#### Règles de gouvernance

La règle qui prime dans la gouvernance des coopératives, c'est la démocratie. La solidarité étant au coeur de l'image du logement que la CODHA aimerait promouvoir, il est très important que tout le monde se sente inclus.

> Dans nos coopératives, nous favorisons la prise de décision lors de réunion où chaque habitant présent pèse une voix. Aucun habitant n'a plus de poids qu'un autre. De cette manière, nous garantissons la démocratie et nous favorisons les discussions entre habitants.

Au niveau de la plateforme PNP, le principe est pareil. Chaque habitant dispose d'un compte et chaque compte a les mêmes droits, ce qui correspond toujours à l'idée d'avoir une coopérative démocratique.

#### Responsables et autres acteurs

Les acteurs de cette activité sont les habitants de la coopérative concernée. Le comité de la CODHA n'intervient normalement pas dans la gestion des coopératives sauf situation exceptionnelle qui nécessite de se pencher sur la stratégie globale de la CODHA.

#### Résultats et utilisation

Nous n'avons malheureusement pas pu avoir accès à un PNP afin de voir la quantité de données présentes par nous-mêmes mais Mr. Käser nous a informé que la plateforme est très utilisée dans toutes les coopératives.


### Analyse des données

#### Source

Les données sont collectées et consultées via l'application PNP. L'application est disponible via une interface WEB, elle est donc disponible sur toutes les plateformes disposant d'un navigateur et d'un accès à internet.

#### Type

Chaque PNP contient les fonctionnalités suivantes :
- Système de stockage de documents afin de déposer / consulter
- Système de rendez-vous pour les différents locaux de la coopérative (salle polyvalente / terrasse)
- Système de ticketing afin de répertorier les incidents et leur gravité
- Système de messages afin de faire parvenir des messages à une certaine liste d'habitants

#### Raison

La plateforme est disponible pour faciliter la communication et la prise de décision. La communication est facilitée grâce aux carnets d'adresses qui permettent de faire parvenir un message rapidement à un certain groupe de personnes. La prise de décision est facilitée grâce à la grande quantitié de documents disponibles sur la plateforme. Il est à tout moment possible de consulter un PV afin de voir comment un problème a été résolu dans le passé.

#### Règles et dispositon

Les données de chaque PNP sont interne à chaque coopérative, il n'y aucun partage entre les différents PNP. Les données peuvent être consultées par n'importe quel habitant de la coopérative mais les données ne sont pas publiques pour autant. Le stockage est géré par la plateforme PNP. Etant une plateforme WEB, on peut considérer que les données sont stockées dans une base de données relationnelle.

D'autres coopératives se sont intéressées à cet outil et ont demandées à la CODHA d'obtenir la plateforme. La CODHA a décidé de partager le code de la plateforme a d'autres coopératives afin que ces dernières puissent également utiliser l'outil. Nous ne savons malheureusement pas comment le code a été cédé à ces autres coopératives (sous licence par exemple).


### Recommandations

Notre recommandation pour cet outil nous a tout simplement sauté aux yeux. En effet, lorsque Mr. Käser nous a dit que les PNP ne communiquent pas entre eux, cela nous a paru complètement contre-productif. Selon nous, la CODHA ainsi que les différentes coopératives gagneraient beaucoup à mettre en place une base de données centrale qui puisse permettre à tous les PNP de stocker leurs données au même endroit.

De cette manière, les bonnes idées qui viennent d'une coopérative seraient partagées à toutes et pourraient être appliquées plus rapidement qu'actuellement. Pour le moment, le partage des informations entre les différentes coopérative se fait chaque année lors d'une grande fête. Cette méthode est certes très conviviale mais elle n'est ni pratique ni rapide.

Si le système est bien adapté, il peut même servir à plus grande échelle, notamment dans le cadre des éco-quartiers qui voient le jour actuellement en ville de Genève.


### Bibliographie

Toutes les informations proviennent de l'interview réalisée avec Mr. Käser, directeur de la maîtrise d'ouvrage à la CODHA.

