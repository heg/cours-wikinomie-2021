--- 
layout: activity
key: geneve_lab_et_open_geneva
title: Open Geneva
tags: [Collaboration, Forum, Données ouvertes, Recommandations]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
--- 
### Situation actuelle

Open Geneva est reconnu comme un acteur majeur de l’écosystème d’innovation de Genève. C’est une association à but non lucratif qui a pour mission de promouvoir et stimuler l’innovation à Genève, c’est-à-dire, favoriser le développement des solutions qui permettent d’améliorer le quotidien des citoyens. 
 
Open Geneva a été créé en 2015 par la Geneva Creativity Center en collaboration avec l’Université de Genève, la HES-SO Genève, le SITG et les TPG pour promouvoir l’innovation ouverte. La première édition a accueilli plusieurs équipes d’étudiants travaillant sur des projets scientifiques et des innovations sociales, techniques liées à l’énergie, santé et mobilité. En 2017, Open Geneva est devenu un Festival de Hackathons. C’est grâce à la Stratégie Numérique de l’Université de Genève que les écoles et centres de recherche, administrations et entreprises ainsi que les structures publiques et privées ont pu être impliqués. Plus de 50 projets innovants ont été développés. 
 
 
#### Règles de gouvernance 
Open Geneva est une association à but non-lucratif constitué d’un comité :
- Président : Thomas Maillart 
- Vice-président : Pierre Mirlesse
- Trésorier : Cyril Jacquet
- Secrétaire : Ségolène Samouiller 
- Responsable des outils digitaux : Helena Borget Dit Vorgeat 
- Responsable des partenariats : Caroline Widmer 
- Coordinatrice des activités : Julia Dallest 
 
Et est également constitué de 26 membres qui représentent chacun une institution différente soit académiques, privées, semi-publiques ou publiques.  
 

#### Responsables et autres acteurs 
Les responsables : le comité d’Open Geneva. 
Les partenaires ont un rôle important car ils sponsorisent les festivals d’innovation, ce sont : 

| CCIG  | Université de Genève | SHB – Swiss House of Brands |
| HUG | Centre de l’innovation | La ville Genève |
| SIG | Genvois Français | Etat de Genève |
| EPFL | Nicomatic | DigitalSwitzerland |
| CERN | Campus Biotech | Citizen Cyberlab |
| RTS | Heidi.News | Geneva Solutions |
| HES-SO | ApiClients | Geneva Tsinghua Initiative |
| | | Fédération des Entreprises Romandes, Genève |

 
#### Résultats et utilisation 
Selon l’analyse du site web de Open Geneva et le rapport annuel 2020 de celui-ci, suite aux hackathons les équipes vont construire des projets basés sur leurs idées.
Les équipes souhaitant poursuivre l'aventure peuvent solliciter Open Geneva qui les accompagnera afin d'éviter un découragement post hackathons. A la fin de cette période, les entrepreneurs auront, en cas de réussite, créé une association ou start-up.

Pour les Meetup, les participants bénéficient d’une meilleure compréhension de l’écosystème d’innovation, et de découvrir le réseau de partenaires accélérateurs. Les rencontres avec ces derniers permettent de créer des collaborations et renforcer le réseau des acteurs de l’innovation. 
 
Finalement, d’autres résultats n’ont pas été observé sur leur site. 
 

### Analyse des données 

#### Source
Open Geneva collecte ses informations avant tout avec les événements organisés, et les activités qui vont avec. Ces derniers consistent en la préparation, la collecte des informations et finalement le traitement des données de l'événement.


_La préparation._
Pour soutenir l'organisation des événements ainsi que la communauté qui désire mettre en œuvre des pratiques de l’innovation constante à l’ère du numérique, Open Geneva et ses partenaires mettent leurs ressources à disposition des participants. 

_Pendant un événement._
Une autre partie des informations sont recueillies pendant les événements eux-mêmes. Voici une liste des événements les plus importants :
- Les hackathons : Les hackathons sont un moyen parfait pour encourager l’intelligence collective. Ces évènements sont toujours liés à une thématique actuelle afin de favoriser l’échange d'idée pour trouver des solutions. Open Geneva organise ou coorganise plusieurs hackathons concernant différents domaines tels que le financement durable, l’intégration des réfugiés dans le marché du travail, les smart city et le sport durant toute l’année. 
- Les rendez-vous du jeudi : Chaque premier jeudi du mois, les citoyens peuvent échanger des idées sur des sujets d’innovation ouverte. Chaque personne peut rejoindre ce rendez-vous via leur site directement. 
- Les ateliers, les confèrences, les portes ouvertes 

En dehors des événements, Open Geneva met aussi d'autres types de plateformes à dispositions qui peuvent leur servir comme source d’information. Un outil important:
- Sparkboard : une plateforme web qui permet d’organiser les hackathons, de documenter les projets, de gérer les suivis et leur visibilité en     ligne et de créer des interfaces pour pouvoir interagir avec les différents acteurs de la communauté.

A côté de ces sources d'informations non-payants, Open Geneva propose des prestations payantes :
- Soutien pour un événement
- Soutien à la stratégie
- Suivi de projets
- Formations diverses


_Traitement des données._
La communication ouverte et l’échange constante entre tous favorisent l’intelligence collective et tiennent donc à cœur à Open Geneva. Pour recevoir des retours, les participants ainsi que les intervenants et les coorganisateurs des événements peuvent les contacter directement ou donner un feedback sur leur site. Les participants de l’Open Geneva Festival ont aussi la possibilité de donner un feedback par des canaux en ligne, par écrit ou par vidéo. 


#### Type
Le type d'information traité est différent et dépend du contexte dans lequel les données ont été récupérées. Par exemple, si une personne donne son feedback en rendant une vidéo d'elle-même et en consent à l’Autorisation de diffusion , même si la vidéo contient des informations personnelles, elle peut quand même être utilisée par Open Geneva.

Notons toutefois, que les idées émergeantes pendant une activité de Open Geneva reste la propriété de son créateur.

Au sein des diverses études conduites en collaboration avec l’université de Genève, des données quantitatives peuvent être collectées.

Cependant, nous ne pouvons pas dire avec certitude de quelle types Open Geneva fait davantage usage. Les informations mentionnées sont issues d'une observation personnelle sur la base du contenu du site internet de Open Geneva.
 

#### Raison
Le but ultime des utilisations des données collectées est de faire avancer la société en termes d'innovation technique et numérique. Avec les feedbacks, les méthodologies d’autres organisateurs et des documents partagés par des experts, Open Geneva prend l’opportunité de constamment améliorer ses événements pour faciliter l’avancement à l’ère du numérique. 

Au-delà, les retours réguliers fournissent les données nécessaires pour la conduite des études pour établir des rapports utiles par rapport à l’exploitation d’un projet innovateur.

En ce qui concerne la distribution des données, Open Geneva met l'ensemble des sources à disposition sur un Google drive publique, auquel tout le public a accès. Il convient de préciser que la majorité des données possédée par Open Geneva restent des données ouvertes.


### Récommandation
OpenGeneva semble être un service très bien structuré et organisé. Les recommandations sont donc plutôt des propositions pour perfectionner leur service. 
Tout d’abord, le crowdsupport est une opportunité extraordinaire pour les projets sans expérience et expertise pour progresser au lancement d’un petit business. Ne pas limiter ce support à une période d'un mois augmentera les chances de réussite de chaque projet. 

Un autre point est la gigantesque quantité d’information disponible sur leur site internet officielle. Pendant qu’elle est très transparent et complet, l’utilisateur simple peut vite se retrouver perdu au milieu de tous ces articles, blogs et rapports. Donner un meilleur emplacement aux informations rendra le site internet plus intuitif et orienté utilisateur afin de faciliter la recherche et la compréhension globale du service.

Finalement, même en ayant une grande visibilité, en termes de chiffre, la plupart des personnes présentes aux événements sont des entrepreneurs ou des experts du domaine traité. Un appel aux citoyens lambda aidera à inclure ces derniers et d'augmenter encore plus la variété de pensée pour un résultat plus adéquat par rapport à l’intelligence collective.


### Bibliographie
Open Geneva. Open Geneva. Dernière modification de la page le 06.13.2021 à 08:12. [Consulté le 13 juin 2021]. [https://www.opengeneva.org](https://www.opengeneva.org)

Open Geneva, 2020, Rapport d’activité septembre 2019 - août 2020 [en ligne] [Consulté le 12 juin 2021]. Disponible à l’adresse : [https://opengeneva.org/wp-content/uploads/2020/12/Annual-Report-2020-Final-Version-1.pdf](https://opengeneva.org/wp-content/uploads/2020/12/Annual-Report-2020-Final-Version-1.pdf)

Open Geneva, 2020, Hackathon Research study primary results [en ligne] [Consulté le 9 juin 2021]. Disponible à l’adresse : [https://drive.google.com/file/d/1gN_UGj6NXmawMB1nSJ9WGLwfCqxhLMfM/view](https://drive.google.com/file/d/1gN_UGj6NXmawMB1nSJ9WGLwfCqxhLMfM/view)
