---
layout: activity
key: geneve_lab_et_open_geneva
title: Genève Lab
tags: [Participation,Service Public,Sensibilisation,Recommandations]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
--- 

### Situation actuelle

Genève Lab est un laboratoire d’innovation publique qui fait des travaux de veille, de prospective ainsi que des expérimentations afin d’appréhender au mieux l’impact du digital. Ces collaborateurs ont pour mission d’accompagner l’administration publique genevoise dans sa transformation à l’air du numérique en s’appuyant sur des leviers d’innovations et de transformation tels que l’intelligence collective, la co-création et la pluridisciplinarité. Pour ce faire, ils interviennent dans le cadre d’un projet à travers différents types d’événements tels que des Meetups ou des formations. 

#### Règles de gouvernance

Ce service a été créé en octobre 2016 et se compte parmi les premiers Living Lab en Suisse et est la première entité publique à rejoindre le réseau European Network of Living Lab. Ils sont soumis à un certain nombre de règles qui prévalent dans l’administration cantonal et ont des procédures propres à Genève lab. Toutefois, étant dans le domaine de l’innovation, il n’y a pas forcément de règlement pour tout surtout sachant qu'il n’existe pas d’autres instances comme celle-ci.

#### Responsables et autres acteurs

L’équipe de Genève Lab est composée de Christopher Larraz, Patrick Genoud et Alexander Barclay. Les différentes parties prenantes sont l’Etat de Genève, les divers intervenants et les citoyens genevois.

#### Résultats et utilisation

Lors d’une intervention dans le cadre d’un projet, un rapport sur mesure est créé puis soumis au mandant sur les bases des données récoltées. Cependant, il n’y a pas de retour formel sur l’utilisation des recommandations par le chef de projet. 
 
Après les événements, les méthodes et les outils utilisés sont publiés dans le blog ainsi que la problématique. Notons toutefois, qu’aucun résultat n’est publié. 

### Analyse des données

#### Source

Afin d’impliquer les divers acteurs de l’innovation à Genève, Genève Lab fournit des formulaires et des questionnaires. De plus, ils organisent cinq événements à savoir : 
 
Genève Lab Meetups : il s’agit de proposer un espace d’échange autour des thématiques au cœur de l’activité de Genève Lab.

  - Public ciblé : les citoyens, le secteur privé, le monde académique, les communes et l’administration cantonale.
  - Accès : Libre et gratuit.

Cafés de la République Numérique : traite des thématiques qui sont relatifs au numérique et au service public. Chacun vient à titre individuel, sur son temps libre et sans rattachement administratif ou hiérarchique.

  - Public ciblé : les collaborateurs du secteur public genevois.
  - Accès : Libre et gratuit.
 
Journées de rencontre : une conférence d’une demi-journée qui a lieu une fois par an. Le thème traité concerne l’usage du numérique et son impact sur la société.

  - Public ciblé : large public de 200 personnes.
  - Accès : Libre et gratuit.

Formations Genève Lab : traite les thématiques sur l’introduction au Design Thinking, des outils de Design Thinking.

  - Public ciblé : les collaborateurs du secteur public genevois.
  - Accès : sur demande.
 
Causeries du jeudi : un évènement lors duquel un intervenant présente ces idées et ces points de vue.

  - Public ciblé : les citoyens, les communes, le secteur privé, le monde académique et l’administration \
  - Accès : libre et gratuit 
 
#### Type

Genève Lab collecte des données quantitatives sur les questionnaires et formulaires ainsi que des données quantitatives pendant les événements. Toutefois, les données collectées sont des retours sur des problématiques spécifiques et les données personnelles ne sont sauvegardées. Celles-ci sont toujours anonymes et ne sont jamais gardées au-delà du mandat. 

#### Raison

Genève Lab essaie d’intégrer le plus possible les citoyens dans leurs réflexions pour ne pas rester cloisonner. En effet, ces derniers sont sollicités par : 
  - La diffusion des évènements sur Meetup.com
  - La publication d’annonces dans les médias
  - La base client du service mandant
  - D’autres solutions

Pour encourager l’intelligence collective, Genève Lab intervient en se basant sur l'objectif et la finalité du projet. Ensuite, selon la problématique, ils vont chercher dans une boîte à outils à laquelle ils se réfèrent pour animer le projet tels que des livres, des références, des tests et des expériences. Toutefois, il arrive qu’ils ne disposent pas d’outils pour un projet par conséquent, ils vont explorer et chercher des nouvelles méthodes à expérimenter. Le but étant d’enseigner une méthodologie adaptée au projet. 
 
Genève Lab agit sur les projets comme une ressource. Leurs interventions se limitent à la reddition d’un rapport qui contiennent des méthodes et outils recommandés. Ceux-ci peuvent être utilisés ou non par le chef de projet. Le but étant que ce dernier puisse acquérir le savoir-faire de Genève Lab et leurs méthodologies, afin qu’ils puissent reproduire ce processus de manière autonome sur un futur projet. 
 
Néanmoins, il y a quatre raisons pour lesquelles Genève Lab refuse d'intervenir : 

  1. Manque de temps ; Genève Lab va solliciter quelques sociétés existantes à Genève. Celles qui peuvent aider regardent ensuite si elles disposent du budget nécessaire.
  2. Démarches alibi ; lorsque quelqu'un sollicite Genève Lab pour un projet mais que la marge de manœuvre est déjà toute décidée. Et, souhaite juste exposer sa collaboration avec Genève Lab.
  3. Manque de compétences ; les collaborateurs de Genève Lab sont ouverts à essayer de nouvelles choses mais refusent lorsque le projet entre dans des domaines dont ils n'ont pas les compétences.
  4. Pas de budget ; Genève Lab ne facture rien en interne et refuse quand des services n'ont pas de budget et veulent uniquement des animateurs de séances. 

#### Règles et disposition

L’utilisation interne à l’organisation concerne Genève Lab pendant toute la durée du projet. Pour ce qui est de l’externe, cela concerne les mandants pour leurs propres projets pour une durée indéterminée. 
 
Les données sont stockées temporairement sur l’application de sondage hébergée sur l’infrastructure de l’administration, accessible uniquement par les ayants droit de l’administration genevoise. 

### Recommandations

Genève Lab reste un service méconnu dans l'administration publique, dû à plusieurs facteurs. Premièrement, le manque de visibilité de leur service peut être améliorer à l’aide d’une meilleure communication interne et ciblée vers les chefs de projets. 
Deuxièmement, leur catalogue des prestations n’est pas clairement défini, ce qui peut représenter un obstacle pour les personnes intéressées. Il faudrait donc mettre en place une description claire et concise des différentes interventions ainsi que leurs fonctionnements.
Troisièmement, demander un retour formel à la fin de chaque intervention permettrait d’améliorer les futures prestations en identifiant les points forts et faibles. 
Dernièrement, une meilleure organisation du site web serait idéal pour avoir une navigation plus agréable afin de faciliter la compréhension de son contenu. 

### Bibliographie

Genève Lab. Ge.ch [en ligne]. Sans date. [Consulté le 13.06.2021]. Disponible à l’adresse : [https://www.ge.ch/dossier/geneve-lab](https://www.ge.ch/dossier/geneve-lab) 

Genève Lab Meetup. Meetup.com [en ligne]. Sans date. [Consulté le 13.06.2021]. Disponible à l’adresse : [https://www.meetup.com/fr-FR/Geneve-Lab-Meetup/](https://www.meetup.com/fr-FR/Geneve-Lab-Meetup/)

Living Lab. Wikipédia : l’encyclopédie libre [en ligne]. Dernière modification de la page le 3 janvier 2021 à 18:18. [Consulté le 13.06.2021]. Disponible à l’adresse : [https://en.wikipedia.org/wiki/Living_lab](https://en.wikipedia.org/wiki/Living_lab)

«Pour innover, il faut développer une culture du partage de l’information» [en ligne]. Le journal de l'Unige n°144, 5 avril - 26 avril 2018, page 8 et 9. [Consulté le 10 juin 2021]. Disponible à l'adresse : [https://www.unige.ch/lejournal/files/9215/3001/6464/journal_web-144.pdf](https://www.unige.ch/lejournal/files/9215/3001/6464/journal_web-144.pdf) 

AZZOLA, Eloïse, 2020. Genève, terrain fertile à l'innovation [en ligne]. Innovation time news, 25 mars 2020. [Consulté le 10 juin 2021]. Disponible à l'adresse :[https://innovation-time.com/geneve-terrain-fertile-a-linnovation/](https://innovation-time.com/geneve-terrain-fertile-a-linnovation/) 
