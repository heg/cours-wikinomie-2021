---
layout: theme
title: "Se faire vacciner Covid-19 et SwissCovid app"
key: Suisse_COVID_app
is_main: true
toc:
- "Contexte"
---


<img src="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/6435c1c2edc4c611689111f28cd107b0/repartition.png" style="width: 100%;">
<div align="center">
<p style="color: grey;"><i> Répartition géographique des cas confirmés Covid-19 en Suisse (Source 8)</i> </p>
</div>

### Contexte
<div align="justify">
<p>Selon l’OMS, « La COVID-19 est la maladie causée par un nouveau coronavirus, le SARS-CoV-2. L’OMS a appris l’existence de ce nouveau virus le 31 décembre 2019 lorsqu’un foyer épidémique de cas de « pneumonie virale » a été notifié à Wuhan, en République populaire de Chine. » (Source 3). La Covid-19 peut engendrer de nombreux symptômes comme notamment la fièvre, la toux, de la fatigue ou de la perte du goût et de l’odorat.</p>
<p>Le premier cas de covid-19 confirmé en Suisse est le 25 février 2020. Il s’agissait d’un Tessinois. Par la suite, d’autres cantons annoncent également les premières contaminations dont Genève. De nombreuses mesures seront prises afin de limiter la propagation de la pandémie dont notamment des confinements, des fermetures de commerce et le port du masque obligatoire.</p>
<p>En juin 2020, l'OSPF en partenariat avec l'EPFL, l'ETHZ et Ubique lance une application qui permet de détecter les contacts avec les personnes aux tests positifs afin d'interrompre les chaînes de transmission .</p>
<p>Le 23 décembre, a lieu la première vaccination en Suisse dans un EMS. C’était une dame âgée de 90 ans.</p> 
<p>En juin 2021, 6 millions de doses ont été administrées.</p>
<p>Le 10 juin 2021, la Suisse compte 696'583 cas. Le canton le plus touché est Zurich avec 113'060 cas, suivi de Vaud avec 85'047, Berne avec 65'411 et Genève avec 63'027. Cependant, Genève est le canton qui compte les plus de cas pour 100'000 habitants avec 12'502,18 cas.</p>
<p>Un certificat COVID-19 approuvé par le conseil fédéral a commencé à être distribué depuis mis-juin 2021. Il permettera de voyager à l'étranger et d'assister à des événements et rassemblements (Source 5 et 6).</p>
<p>En Suisse, on compte 10'852 décès depuis le début de la pandémie et 3'778'635 dans le monde.</p>
</div>

### Bibliographie
<ol>
    <li>« COVID Live Update: 175,234,890 Cases and 3,778,695 Deaths from the Coronavirus - Worldometer ». Consulté le 10 juin 2021. <a href="https://www.worldometers.info/coronavirus/"> https://www.worldometers.info/coronavirus/</a></li>
    <li>« La vaccination contre le Covid-19 a commencé en Suisse - rts.ch - Suisse ». Consulté le 10 juin 2021. <a href="https://www.rts.ch/info/suisse/11848533-la-vaccination-contre-le-covid19-a-commence-en-suisse.html">https://www.rts.ch/info/suisse/11848533-la-vaccination-contre-le-covid19-a-commence-en-suisse.html</a></li>
    <li>« Maladie à coronavirus 2019 (COVID-19) : ce qu’il faut savoir ». Consulté le 10 juin 2021. <a href="https://www.who.int/fr/news-room/q-a-detail/coronavirus-disease-covid-19">https://www.who.int/fr/news-room/q-a-detail/coronavirus-disease-covid-19</a></li>
    <li>« Rétrospective 19-20 – Et soudain, le Covid-19 a chamboulé notre vie quotidienne | 24 heures ». Consulté le 10 juin 2021. <a href="https://www.24heures.ch/et-soudain-le-covid-19-a-chamboule-notre-vie-quotidienne-826246563119">https://www.24heures.ch/et-soudain-le-covid-19-a-chamboule-notre-vie-quotidienne-826246563119</a></li>
    <li>rts.ch. « Comment se procurer un certificat Covid dans son canton? » InfoSport, 7 juin 2021.<a href="https://www.rts.ch/info/suisse/12258476-comment-se-procurer-un-certificat-covid-dans-son-canton.html"> https://www.rts.ch/info/suisse/12258476-comment-se-procurer-un-certificat-covid-dans-son-canton.html
</a></li>
    <li>rts.ch. « Le certificat Covid sera le sésame indispensable pour voyager, danser ou courir les festivals ». InfoSport, 19 mai 2021. <a href="https://www.rts.ch/info/suisse/12211183-le-certificat-covid-sera-le-sesame-indispensable-pour-voyager-danser-ou-courir-les-festivals.html">https://www.rts.ch/info/suisse/12211183-le-certificat-covid-sera-le-sesame-indispensable-pour-voyager-danser-ou-courir-les-festivals.html</a>
</li>

  <li>« COVID19 à Genève. Données cantonales ». Consulté le 2 juin 2021. <a href ="https://infocovid.smc.unige.ch/">https://infocovid.smc.unige.ch/</a> </li>

   <li>« COVID-⁠19 Suisse | Coronavirus | Dashboard ». Consulté le 13 juin 2021. <a href="https://www.covid19.admin.ch/fr/epidemiologic/case">https://www.covid19.admin.ch/fr/epidemiologic/case</a></li>
</ol>
