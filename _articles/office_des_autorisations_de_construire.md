---
layout: theme
title: "Office des autorisations de construire"
key: office_des_autorisations_de_construire
is_main: true
toc:
- "Contexte"
---

### Contexte
 Selon son [site](https://www.ge.ch/organisation/office-autorisations-construire), l'office des autorisations de construire est chargée d'instruire et de se prononcer sur les dossiers de demandes d'autorisations de construire pour toutes les constructions, transformations, démolitions sur le territoire genevois.

Elle veille, par ailleurs, au respect des règles et lois relatives au domaine de la construction au sens plus large, allant de la protection de la santé des ouvriers sur les chantiers et de leur sécurité, ainsi que celle du public, à la délivrance de prestations essentiellement liées aux domaines de la sécurité et de la prévention des incendies et de la salubrité.

A cette fin, et dans le cadre de la numérisations de ses sevices, l'administration a mis en place divers services permettant une interaction informatisée, notamment :

 * Les demandes d'autorisations de construire par procédure accélérée (APA)
 * Les demandes d'autorisations de démolition par procédure accélérée (DPA)
 * Le suivi des dossiers (SAD-Cons)

