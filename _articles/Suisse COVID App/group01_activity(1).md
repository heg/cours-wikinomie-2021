---
layout: activity
key: Suisse_COVID_app
title: Se faire vacciner Covid-19
tags: [information, santé, dossier médical, statistiques]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
<div align="justify">
<p>La vaccination est actuellement ouverte aux personnes âgées de plus 16 ans résidant à Genève ou frontalier souscrivant une assurance maladie. Les personnes âgées de plus de 44 ans sont prioritaires dans l’obtention d’un rendez-vous, puis c’est le système de « premier inscrit – premier servi ». Ce service est gratuit (prise en charge par l’assurance maladie).</p>

<p>La prise de rendez-vous pour tous les centres se fait par le site web de l’état de Genève : ge.ch.</p>
 
<p>Les vaccinations se font dans des centres spécialisés. Il y a actuellement 12 centres à Genève dont deux centres pilotes en collaboration avec des pharmacies. Il y a actuellement 10 centres à Genève et deux centres pilotes en collaboration avec des pharmacies qui est réservé pour les personnes les plus vulnérable en priorité.</p>
</div>

#### Règles de gouvernance
<div align="justify">
<p>Le public a accès aux données qui ont été anonymisés au préalable. Elles sont publiées sur le Dashboard du covid-19 sur le site web de la confédération Suisse (Source 2). Il est aussi possible de les télécharger en format CSV ou JSON depuis le site. Ces fichiers téléchargeables sont mis à jour tous les 5 à 10 jours.</p> 
</div>

#### Responsables et autres acteurs
<img src="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/be5ce25f9be2876de8c713e529c48a9e/schema.png" style="width: 100%;">
<div align="center">
<p style="color: grey;"><i>Schéma des responsables et acteurs</i> </p>
</div>

<table style="width:100%">
  <tr style="background-color: Gainsboro;" >
    <th>Acteur</th>
    <th>Rôle(s)</th> 
  </tr>
  <tr>
    <td>Office fédéral de la santé publique (OSPF)</td>
    <td>
      - Gestion des statistiques Covid-19 pour toute la Suisse 
      <br/> - Gestion des statistiques SwissCovid 
      <br/> - Gestion des statistiques Vaccinations pour toute la Suisse 
    </td> 
  </tr>
  <tr>
    <td>Site web cantonal, ge.ch (OneDoc)</td>
    <td>
      - Gestion des statistiques de vaccination pour le canton de Genève
      <br/>- Prise de rendez-vous pour se faire vacciner pour le canton de Genève 
    </td> 
  </tr>
  <tr>
    <td>Centre de vaccination</td>
    <td>
      - Vaccine les personnes 
    </td> 
  </tr>
  <tr>
    <td>Pharmacie cantonale</td>
    <td>
      - Commande les doses de vaccins 
    </td> 
  </tr>
</table>

#### Résultats et utilisation
<div align="justify">
<p>Les données qui sont collectées par l’OFSP sont entièrement anonymisés. Elles vont ensuite être affiché sur le Dashboard (Source 2). Les statistiques de Genève sont disponibles sur le site (Source 4).</p>

<p>Ses données constituent une base pour prendre des décisions, mais il s’agit seulement d’un élément parmi d’autres.</p>
</div>

### Analyse des données
#### Sources
<div align="justify">
<p>Les données sont collectées par les centres de vaccinations et enregistrées sur ge.ch (Pour le canton de Genève) avec le web application One Doc. Il s’agit d’un site web pour prendre des rendez-vous avec des professionnels de la santé en Suisse. Elle est directement intégrée à ge.ch.</p>

<p>La base logistique de l’armée (BLA) (Source 6), qui s’occupe du domaine de la santé lors de situation extraordinaire d’ampleur national (pandémie et évènements majeurs), qui fournit des informations à l’OFSP.</p>

<p>Lorsque l’OFSP récupère les données, il n’y a pas uniquement une personne qui traite les données, mais un groupe de travail qui gèrent l’ensemble des données liées à la pandémie. Les analyses qui sont effectuées avec les données de vaccination sont réalisées avec le logiciel R.</p>
</div> 

#### Type
<table style="width:100%">
  <tr style="background-color: Gainsboro;">
    <th>Acteur</th>
    <th>Donnée(s)</th> 
  </tr>
  <tr>
    <td>Office fédéral de la santé publique (OSPF)</td>
    <td>Vaccination effectuée : 
      <br/> - Âge 
      <br/> - Canton de résidence 
      <br/> - Motif de vaccination 
      <br/> - Type d’institution administrant  le vaccin  
      <br/> - Type de vaccin
      <br/> - Date d’administration
      <br/> - Nombre de doses
    </td> 
  </tr>
  <tr>
    <td>Site web cantonal, ge.ch (OneDoc)</td>
    <td>Rendez-vous vaccination :
      <br/> - Prénom
      <br/> - Nom
      <br/> - Date de Naissance
      <br/> - Numéro d’assurance
      <br/> - Risque de santé
      <br/> - Centres de vaccinations de préférence
      <br/> - Genre
      <br/> - Adresse électronique
      <br/> - Numéro de téléphone
    </td> 
  </tr>
   <tr>
    <td>Centres de vaccinations </td>
    <td>Rendez-vous vaccination
      <br/> - Prénom
      <br/> - Nom
      <br/> - Date de Naissance
    </td> 
  </tr>
   <tr>
    <td>Pharmacie cantonale</td>
    <td>
      - Nombre de doses administrées par chaque centre de vaccination
    </td> 
  </tr>
</table>

#### Raison
<div align="justify">
<p>C’est le rôle du conseil fédéral, pendant cette période de crise, de prendre les meilleures décisions pour protéger la population Suisse. Pour ce faire, le conseil fédéral s’aide d’avis d’expert et des données sur la situation générale et la progression du Covid-19 (nombre de cas, nombre de personnes vaccinées, etc.). Les décisions prises sont transmises à la population lors de conférences de presse hebdomadaires.</p>
</div>

#### Règles et dispositon
<div align="justify">
<p>One doc est d’un logiciel privé, les données sont confidentielles et sont partagées uniquement entre le patient et le professionnel de la santé (Source 5). L’ensemble de leurs solutions sont hébergées en suisse, ils sont donc soumis à la RGPD. Le site possède aussi le label « Swiss made software » qui certifie la qualité du service (Source 8).</p>

<p>Le logiciel R est sous licence GNU GPL. C’est-à-dire qu’il est libre à la modification et le code source est disponible, mais toutes modifications doivent être partagé à la communauté (Source 7).</p>
</div>

### Recommandations
<div align="justify">
<p>Le travail effectué par l’OSPF est très important pour tenir la population Suisse informée de la progression de la pandémie. Cela permet aussi aux personnes en charge des décisions de s’aiguiller. La présentation et la transformation des données est de qualité et détaillée. Nous ne savons pas comment l’épidémie évoluera, mais les nouvelles souches pourraient présenter un défi majeur pour la vaccination.</p>

<p>La prise en charge pour la vaccination est simple et efficace (ge.ch). Le fait qu’elle soit gratuite est un avantage considérable. Nous recommandons à tous de prendre rendez-vous car il y a un temps d’attente.</p>
</div>

### Bibliographie
<ol>
  <li>ge.ch. « Se faire vacciner ». Consulté le 31 mai 2021. <a href="https://www.ge.ch/node/23333">https://www.ge.ch/node/23333</a> </li>
  <li>« COVID-⁠19 Suisse - Coronavirus - Dashboard ». Consulté le 1 juin 2021.<a href="https://www.covid19.admin.ch/fr/overview"> https://www.covid19.admin.ch/fr/overview</a></li>
  <li>OFSP, Office fédéral de la santé publique. « Coronavirus : vaccin ». Consulté le 1 juin 2021. <a href="https://www.bag.admin.ch/bag/fr/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/impfen.html">https://www.bag.admin.ch/bag/fr/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/impfen.html </a></li>
  <li>« COVID19 à Genève. Données cantonales ». Consulté le 2 juin 2021. <a href ="https://infocovid.smc.unige.ch/">https://infocovid.smc.unige.ch/</a> </li>
  <li>OneDoc. « Terms and Conditions ». Consulté le 7 juin 2021. <a href="https://www.onedoc.ch">https://www.onedoc.ch</a> </li>
  <li>« Base logistique de l’armée BLA ». Consulté le 7 juin 2021. <a href="https://www.vtg.admin.ch/fr/organisation/bla.html">https://www.vtg.admin.ch/fr/organisation/bla.html</a> </li>
  <li>« Licence publique générale GNU ». In Wikipédia, 14 avril 2021.<a href="https://fr.wikipedia.org/w/index.php?title=Licence_publique_g%C3%A9n%C3%A9rale_GNU&oldid=181899893"> https://fr.wikipedia.org/w/index.php?title=Licence_publique_g%C3%A9n%C3%A9rale_GNU&oldid=181899893</a> </li>
  <li>OneDoc. « La technologie au service du système de santé Suisse ». Consulté le 7 juin 2021. <a href="https://about.onedoc.ch/fr/">https://about.onedoc.ch/fr/ </a></li>
  <li><a href="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/0fa7388bffee695583591b33ede4f7f1/InterviewGregoireGogniatOFSP.pdf">Interview Grégoire Gogniat, porte-parole OFSP, réalisé le 9 mai 2021</a> </li>
  <li><a href="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/24e2c1c3778ac8804ffbd105770a9c21/InterviewResponsableCentreVaccination.pdf">Interview Responsable centre vaccination, réalisé le 23 mai 2021</a> </li>
</ol> 
