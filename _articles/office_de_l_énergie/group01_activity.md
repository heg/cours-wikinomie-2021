---
layout: activity
key: office_de_l_énergie
title: GEnergie
tags: [collaboration, service public, environnement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
<div align="justify">
<img src="https://www.ge.ch/media/d7/genergie_couleur_short.jpg" style="margin-left: 15px; border: 0;" align="right" width="230" height="230" />
<p>Grâce notamment aux recettes de la taxe CO<sub>2</sub>, l’État a pu dégager une enveloppe d’environ 35 millions de francs pour l’année 2020. L’idée du projet est de redistribuer cet argent aux citoyens sous la forme de subventions, si les installations de ces derniers répondent à une série de critères, spécifiés dans un formulaire à remplir. Le but est d’encourager voire d’accompagner les citoyens vers la rénovation d’anciennes infrastructures afin de réduire leur consommation.</p>
</div>


#### Règles de gouvernance
<div align="justify">
<p>La manipulation des données liées aux formulaires reste dans un cadre restreint, notamment car ces données tombent sous l’égide de la LIPAD, et n’est pas de leur ressort pour la majeure partie.</p>
<p>En revanche, l’OCEN transmet des données publiques sur la dépense par d’autres moyens et sous d’autres formes. Certaines données sont notamment cartographiées par l’intermédiaire du Système d’information du territoire à Genève (SITG). De plus, et une fois par année, une conférence de presse est tenue durant laquelle l’Etat transmet au public la répartition de l’argent alloué, le type de subvention concernée, etc.</p>
</div>

#### Responsables et autres acteurs
<div align="justify">
<ul>
  <li>Services industriels de Genève (SIG)</li>
  <li>Office cantonal de l’énergie (OCEN)</li>
  <li>Système d'information du territoire à Genève (SITG)</li>
  <li>Les entreprises mandatées dans le cadre du projet</li>
  <li>Les citoyens participants</li>
</ul>
</div>

#### Résultats et utilisation
<div align="justify">
<p>Le projet se porte normalement. La situation liée au Covid-19 a engendré un léger creux dans les demandes et le processus d’analyse des installations, mais les participants se sont bien adaptés ce qui a permis au projet de reprendre normalement. L’enveloppe a été entièrement dépensée.</p>
</div>


### Analyse des données

#### Source
<div align="justify">
<ul>
  <li>Formulaire de demande de subventions</li>
  <li>Relevés de consommations</li>
</ul>
</div>

#### Type
<div align="justify">
<ul>
  <li>Par le formulaire : données personnelles, énergétiques et caractéristiques sur le bâtiment.</li>
  <li>Par les relevés : la consommation énergétique brute et la surface de référence énergétique sont saisies via une application (Indice) puis stockées dans une base de données.</li>
</ul>
</div>

#### Raison
<div align="justify">
<ul>
  <li>Pour le formulaire : mise en relation des citoyens participants avec l’Office.</li>
  <li>Pour les relevés : afin de monitorer l’évolution de la situation et de dresser des statistiques.</li>
</ul>
</div>

#### Règles et disposition
<div align="justify">
<ul>
  <li>Pour le formulaire : dans un cadre restreint aux particuliers, car les données personnelles sont protégées par la LIPAD.</li>
  <li>Pour le relevé : pour certaines métriques, mises à disposition sous forme de base de données accessible sur le site du SITG. Le partage de ces données suit la démarche OpenData, dans une optique de partage et de transparence.
Ces données sont disponibles sous forme d’une carte légendée des bâtiments (identifiés par leur EGID) et via des bases de données contenant des informations sur la consommation et l’émission des bâtiments.
Ces données sont structurées sous forme de tableurs et de vecteurs géospatiaux, notamment dans les format .CSV, .GBD et .SHP (★★★ sur <a href="https://5stardata.info/en/">l’échelle de qualité</a> des données ouvertes de Tim Berners-Lee).</li>
</ul>
</div>


### Recommandations
<div align="justify">
<p>Le projet se basent sur une approche rétroactive en attendant les demandes de subventions des entreprises et citoyens, nous pensons que l’Etat devrait être proactif et aller à la rencontre des propriétaires d’infrastructures datées et peu durables. Ce faisant, il pourrait convaincre ces acteurs d’améliorer leur installation en leur proposant des subventions attrayantes, ce qui pourrait passer par une augmentation de l’enveloppe allouée au projet.</p>
</div>

### Interview
<div align="justify">
<p>Lors de nos recherches, nous avons pu poser quelques questions à Monsieur <strong>Damien CHIFFELLE</strong>, Chargé de projet au sein de l’Office cantonal de l’énérgie (OCEN), lors d’un échange par téléphone. À noter qu'il s'agit d'un résumé et non d'une retranscription stricte de l'entrevue et que Monsieur CHIFFELLE a accepté d'être cité par nom.</p>
</div>

**Où en est le projet ? Le Covid a-t-il impacté la quantité de demandes et la possibilité de répondre à ses demandes ?**
> _À priori, le projet se porte normalement et n’a pas vraiment été affecté par le Covid, car les demandes issues du projet sont souvent fixé(e)s des années à l’avance._ 

**NOTE :** Monsieur Chiffelle avait demandé ensuite confirmation à un collègue, inspecteur pour l'attribution des subventions, qui a confirmé que malgré un creux, le budget avait été entièrement dépensé avec des moyens adaptés.

**Au niveau du formulaire de demande de subvention, nous l'avons testé fictivement et aimerions savoir quelles données sont récupérées et ce que vous en faites-vous après coup. Les liez-vous avec la consommation du SIG? Utilisez-les vous à des fins statistiques pour monitorer l’évolution post-subvention ?**
> _Le manipulation des données liées aux formulaires reste dans un cadre restreint (notamment car ces données sont sous l’égide de la LIPAD) et n’est pas de leur ressort pour la majeure partie. En revanche, l’OCEN transmet des données publiques sur la dépense par d’autres moyens et sous d’autres formes : certaines données sont notamment cartographiées par l’intermédiaire du Système d’information du territoire à Genève (SITG). Par exemple, les bâtiments genevois catégorisés par leur identificateur de bâtiment (EGID) se voient ajouter des feuilles de données, contenant des informations comme l’indice de dépense de chaleur (IDC)._

**NOTE :** Monsieur Chiffelle nous a ensuite envoyé des exemples par mail, disponibles sous plusieurs formes, notamment de tableurs Excel.

**Le public n’ayant pas accès aux données brutes dont vous disposez, mettez-vous à disposition du public un retour sur le projet sous une autre forme et si oui, laquelle ?**
> _Une fois par année, une conférence de presse est tenue durant laquelle l’Etat transmet au public la répartition de l’argent alloué, le type de subvention concerné, etc._

**Que l’état compte-t-il faire suite aux résultats du projet ? Peut-on envisager une augmentation de l’enveloppe allouée à ce genre de politiques ?**
> _Le plan validé chaque année, puis la Confédération décide de ce qui passe ou pas. « Les subventions c’est la carotte, mais après il y a le bâton » : ce qui est interdit demain ne sera évidemment pas subventionné. La conjoncture peut également décider de la suite des évènements._

**Est-ce que la complexité du système politique suisse compromet la bonne marche de votre projet ?**
> _Genève est relativement en avance en termes de politique énergétique, notamment en comparaison nationale. Avoir un vert au gouvernement cantonal (Antonio Hodgers) aide évidemment dans ce sens. Si la chaleur est d’ordre cantonal, l’électricité est traitée au niveau fédéral ce qui peut parfois brider Genève qui essaie d’aller vite dans ses politiques écologiques._

### Bibliographie
<div align="justify">
<ul>
<li>Etat de Genève. <i>GEnergie 2050: des mesures concrètes pour accélérer la transition énergétique du canton. </i>[En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://www.ge.ch/document/genergie-2050-mesures-concretes-accelerer-transition-energetique-du-canton">https://www.ge.ch/document/genergie-2050-mesures-concretes-accelerer-transition-energetique-du-canton</a></li>
<li>GEnergie. <i>Des subventions pour améliorer l’efficacité énergétique des bâtiments à Genève.</i> [En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://www.genergie2050.ch/des-subventions-pour-ameliorer-lefficacite-energetique-des-batiments-geneve">https://www.genergie2050.ch/des-subventions-pour-ameliorer-lefficacite-energetique-des-batiments-geneve</a></li>
<li>SITG. <i>Catalogue de données.</i> [En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://ge.ch/sitg/sitg_catalog/sitg_donnees?keyword=idc&topic=tous&datatype=tous&service=tous&distribution=tous&sort=auto#">https://ge.ch/sitg/sitg_catalog/sitg_donnees?keyword=idc&topic=tous&datatype=tous&service=tous&distribution=tous&sort=auto#</a></li>
</ul>
</div>
