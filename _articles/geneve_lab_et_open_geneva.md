---
layout: theme
title: "Genève Lab et Open Geneva"
key: geneve_lab_et_open_geneva
is_main: true
toc:
- "Contexte"
---


### Contexte
« Pour innover, il faut développer une culture du partage de l’information » page 8, Le journal de l’Unige. C’est dans cette idée que des services et associations axés sur l’intelligence collective comme levier d’innovation ont vu le jour ces dernières années.
<img src="http://www.lescahiersdelinnovation.com/wp-content/uploads/2015/04/mass-customization.jpg" style="margin-left: 15px; border: 0;" align="right" width="230" height="230" />

L'intelligence collective représente la capacité intellectuelle d'une communauté d'individus à réaliser des tâches complexes grâce aux interactions entre ses membres. 
Le but étant de faire de la maïeutique, qui consiste à emmener le public à donner forme à sa pensée. 

C'est ainsi que Genève Lab, service de l'administration genevoise ayant pour but de sensibiliser l’administration et la population genevoise sur les opportunités liées au numérique, travaille. En effet, celui-ci va intervenir, généralement à la demande d'un chef de projet de l'administration genevoise, sur une problématique en récoltant les retours des administrés. Ce processus se fait aussi bien par des formulaires anonymes qu'à travers des ateliers de cocréation. Puis, les collaborateurs de Genève Lab vont rédiger un rapport contenant ces données et des recommandations.

Outre ce service, nous pouvons citer Open Geneva, une association à but non lucratif ayant pour mission de promouvoir et stimuler l'innovation ouverte. Cette dernière va, entre autres, organiser des hackathons lors desquels l'intelligence collective est mise en pratique afin de développer des solutions pratiques à des problèmes concrets.

Nous pouvons ainsi voir que l'avenir de l'innovation se trouve dans l'intelligence collective comme le démontre l'étude sur les hackathons pour l'apprentissage et la co-création à des fins d'innovation de Open Geneva. Sur 98 participants, « 70% ont indiqué que la raison majeure de leur participation au hackathon était pour co-créer avec d'autres personnes » et « 70% sont tout à fait d'accord pour dire qu'une cause de durabilité plus élevée dans un hackathon est une forte motivation pour participer ».
